package demo.db;

import demo.db.config.Config;
import demo.impl.LikeDaoImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.Jedis;

public class LikeDaoTest {

    Config config = new Config(6380);
    LikeDao likeDao;
    Jedis jedis;
    String key = "test";

    @Before
    public void setUp() throws Exception {
        config.startRedisServer();
        jedis = config.getConnection();
        likeDao = new LikeDaoImpl(jedis);
        jedis.del(key);
    }

    @After
    public void tearDown() throws Exception {
        config.stopServer();
    }

    @Test
    public void increment() throws Exception {
        likeDao.increment("test");
        Assert.assertEquals(likeDao.getCountByID("test"), 1L);
    }

    @Test
    public void getCountByID() throws Exception {
        likeDao.increment("test");
        likeDao.increment("test");
        likeDao.increment("test");
        likeDao.increment("test");
        likeDao.increment("test");
        likeDao.increment("test");
        Assert.assertEquals(likeDao.getCountByID("test"), 6L);

    }

}