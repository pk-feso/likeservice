package demo.db.config;

import redis.clients.jedis.Jedis;
import redis.embedded.RedisServer;

import java.io.IOException;

public class Config {

    private final RedisServer redisServer;

    public Config(int port) {
        try {
            redisServer = new RedisServer(port);
        } catch (IOException e) {
            throw new RuntimeException("Cannot start redis server");
        }
    }

    public  void startRedisServer()  {
          redisServer.start();
    }

   public void stopServer() {
       redisServer.stop();
   }

    public  Jedis getConnection() {
        return new Jedis("localhost");
    }



}
