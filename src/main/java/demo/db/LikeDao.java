package demo.db;

public interface LikeDao {

    void increment(String id);

    long getCountByID(String id);

}
