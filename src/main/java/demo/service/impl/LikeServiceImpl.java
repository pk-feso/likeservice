package demo.service.impl;

import demo.db.LikeDao;
import demo.service.LikeService;

public class LikeServiceImpl implements LikeService {

    private final LikeDao dao;

    public LikeServiceImpl(LikeDao dao) {
        this.dao = dao;
    }

    public void like(String playerId) {
        dao.increment(playerId);
    }

    public long getLikes(String playerId) {
        return dao.getCountByID(playerId);
    }

}
