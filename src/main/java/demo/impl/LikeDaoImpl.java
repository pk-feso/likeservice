package demo.impl;

import demo.db.LikeDao;
import redis.clients.jedis.Jedis;

public class LikeDaoImpl implements LikeDao {

    private final Jedis redis;


    public LikeDaoImpl(Jedis redis) {
        this.redis = redis;
    }


    public void increment(String id) {
        redis.incr(id);

    }

    public long getCountByID(String id) {
        return Long.parseLong(redis.get(id));
    }
}
